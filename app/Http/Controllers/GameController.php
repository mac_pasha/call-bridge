<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Session;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;

class GameController extends Controller
{
    public function join()
    {
        //print_r(Cache::get('Players'));
        //print_r(Session::get('corePlayer'));
        return view('pages.join');
    }
    public function joinTeam()
    {
        $playerName = Input::get('playerName');
        $expiresAt = Carbon::now()->addMinutes(36000);

        $playerNames = json_decode(Cache::get('Players'));

        if(empty($playerNames))
            $playerNames = [];

        array_push($playerNames, $playerName);

        Cache::put('Players', json_encode($playerNames), $expiresAt);
        Session::set('corePlayer', $playerName);

        print_r(Cache::get('Players'));

        if (sizeof($playerNames)==4) {
            $this->_shuffle();
        }
        return view('pages.waiting');
    }

    public function checkteam()
    {
        $data = json_decode(Cache::get('Players'));;
        return [
            'count' => count($data),
            'data' => $data,
        ];
    }

    public function play()
    {
        return view('pages.board');
    }

    public function shuffleAgain ()
    {
        $this->_shuffle();
    }

    public function shuffle()
    {
        return view('pages.board')->with('playerCards', Cache::get('PlayerCards'))->with('bids', Cache::get('playerNameswithBid'));
    }

    public function startPlay($bid)
    {
        $expiresAt = Carbon::now()->addMinutes(36000);
        //Cache::put('PlayersBid', json_encode($playerNames), $expiresAt);

        $playerNameswithBid = Cache::get('playerNameswithBid');
        if(empty($playerNameswithBid))
            $playerNameswithBid = [];

        $playerName = Session::get('corePlayer');
        $playerNameswithBid[$playerName] = $bid;

        Cache::put('playerNameswithBid', $playerNameswithBid, $expiresAt);
        return Cache::get('playerNameswithBid');
    }

    public function checkBids()
    {
        return Cache::get('playerNameswithBid');
    }

    public function cardPlayed($suite, $rank)
    {
        $expiresAt = Carbon::now()->addMinutes(36000);

        $playedCard = Cache::get('playedCard');
        if(empty($playedCard))
            $playedCard = [];

        $playerName = Session::get('corePlayer');
        $playedCard[$playerName] = [ "suite" => $suite, "rank" => $rank];

        Cache::put('playedCard', $playedCard, $expiresAt);
        return Cache::get('playedCard');
    }

    public function checkPlay()
    {
        return Cache::get('playedCard');
    }

    public function calculatePoint()
    {
        $s = Cache::get('playedCard');
        $expiresAt = Carbon::now()->addMinutes(36000);
        $winnerName = $this->checkWinner($s);

        $points = Cache::get('playerPoint');
        $points[$winnerName] += 1;
        Cache::put('playerPoint', $points, $expiresAt);
        Cache::put('playedCard', [], $expiresAt);

        return [
            "points" => Cache::get('playerPoint'),
            "next" => $winnerName
        ];
    }

    public function checkWinner($cardsArray)
    {
        $winner = $firstPlayer  =   current(array_keys($cardsArray));
        $names = array_keys($cardsArray);
        array_shift($names);
        $firstCard = array_shift($cardsArray);
        $cardsArray = array_combine($names, $cardsArray);

        $playedSuite = $firstCard['suite'];
        $cardIndex  =  array_search($firstCard['rank'], Cards::$RANKS, true);

        foreach ($cardsArray as $key => $card){
            if ($card['suite'] == $playedSuite){
                $currentIndex = array_search($card['rank'], Cards::$RANKS, true);
                if($cardIndex > $currentIndex){
                    $cardIndex = $currentIndex;
                    $winner = $key;
                }
            }else if($card['suite'] == 'Spades'){
                $playedSuite = 'Spades';
                $cardIndex  =  array_search($card['rank'], Cards::$RANKS, true);
                $winner = $key;
            }
        }
        return $winner;
    }

    private function _shuffle()
    {

        $expiresAt = Carbon::now()->addMinutes(36000);

        $cards = [];
        for ($i = 0; $i < sizeof(Cards::$TYPES); $i++) {
            for ($j = 0; $j < sizeof(Cards::$RANKS); $j++) {
                $card = new Cards(Cards::$TYPES[$i], Cards::$RANKS[$j]);
                array_push($cards, $card);
            }
        }

        $shuffledCards = $cards;
        shuffle($shuffledCards);

        /*
         * Create Players
         */
        $players = [];
        $palyerListFromCaches = Cache::get('Players');
        $palyerListFromCaches = json_decode($palyerListFromCaches);

        foreach ($palyerListFromCaches as $palyerListFromCache) {
            array_push($players, new PlayerCards($palyerListFromCache));
        }

        $j = 0;
        for ($i = 0; $i < 52; $i++) {
            $players[$j++]->addCard($shuffledCards[$i]);
            if ($j == 4) {
                $j = 0;
            }
        }

        foreach ($palyerListFromCaches as $playerName) {
            $playerPoint[$playerName] = 0;
        }

        Cache::put('PlayerCards', ($players), $expiresAt);
        Cache::put('playerPoint', $playerPoint, $expiresAt);
    }


}
