<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Cards extends Controller
{
    public static $TYPES = [
        'Diamond',
        'Hearts',
        'Spades',
        'Clubs'
    ];

    public static $RANKS = ['A', 'K', 'Q', 'J', '10', '9', '8', '7', '6', '5', '4', '3', '2'];

    //rank and type
    private $type;
    private $rank;

    public function __construct($type, $rank)
    {
        $this->type = $type;
        $this->rank = $rank;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getRank()
    {
        return $this->rank;
    }

    public function getImage()
    {
        $images = [
            'Diamond'   => "images/diamond.png",
            'Hearts'    => "images/hearts.png",
            'Spades'    => "images/spade.png",
            'Clubs'     => "images/clubs.png"
        ];
        return $images[$this->type];
    }
}
