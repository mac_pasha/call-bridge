<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PlayerCards extends Controller
{
    public $name;
    public $cardsInHand;

    public function __construct($name)
    {
        $this->name = $name;
        $this->cardsInHand = [];
    }

    public function getName()
    {
        return $this->name;
    }

    public function addCard($card)
    {
        array_push($this->cardsInHand, $card);
    }

    public function getCardsInHand()
    {
        return $this->cardsInHand;
    }
}
