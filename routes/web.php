<?php

use Illuminate\Support\Facades\Cache;

Route::get('join', 'GameController@join');
Route::post('join', 'GameController@joinTeam');
Route::get('test-players', 'GameController@checkteam');
Route::get('play', 'GameController@play');

Route::get('shuffle', 'GameController@shuffle');
Route::get('bidPlace/{bid}', 'GameController@startPlay');
Route::get('check-bids', 'GameController@checkBids');

Route::get('players-bid', 'GamePointController@playersBid');

Route::get('play-card/{suite}/{rank}', 'GameController@cardPlayed');
Route::get('check-play', 'GameController@checkPlay');

Route::get('calculate', 'GameController@calculatePoint');

Route::get('check-cache', function()
{
    echo "<pre>";
    print_r(json_decode(Cache::get('Players'), true));
    echo "<br>";
    print_r(Session::get('corePlayer'));
    echo "<br>";
    print_r(Cache::get('playerNameswithBid'));
    echo "<br>";
    print_r(Cache::get('playedCard'));
    echo "<br>";
    print_r(Cache::get('playerPoint'));
    die;
});

Route::get('/clear', function() {
    Cache::forget('Players');
    Cache::forget('PlayerCards');
    Cache::forget('playerNameswithBid');
    Cache::forget('playedCard');
    Cache::forget('playerPoint');
    \Session::put('corePlayer', null);
});

