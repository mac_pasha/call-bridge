@extends('layouts.application')

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <form action='join' method='post'>
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="playerName">Player Name</label>
                    <input type="text" class="form-control" name="playerName" id="playerName" placeholder="Your Name">
                </div>

                <button type="submit" class="btn btn-default">Submit</button>
            </form>
        </div>
    </div>
@stop
