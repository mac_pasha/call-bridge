@extends('layouts.application')

@section('content')

    <div class="row" style="position: fixed">
        <div class="col-md-12">
            <table id="playerBids">

            </table>
        </div>

        <div class="col-md-12">
            <table id="playerCollected">

            </table>
        </div>
    </div>

    <div class="row">
        <?php $i=1; ?>
        @foreach($playerCards as $player)
            <div class="user-panel @if($i == 1 || $i == 4) {{ 'col-md-6 col-md-offset-3' }} @elseif($i == 3){{ 'col-md-4 col-md-offset-4' }} @else{{ 'col-md-4' }} @endif">
                <div class="panel panel-default text-center">
                    <div class="panel-heading">{{ $player->name}}</div>
                    <div class="panel-body">
                        <p class="text-left">
                            Player's Bid Point :
                        </p>
                        @if(Session::get('corePlayer') === $player->name)
                            @foreach($player->cardsInHand as $card)
                                <div class="card-holder" data-suite="{{ $card->getType() }}" data-rank="{{ $card->getRank() }}">
                                    <img src="{{ $card->getImage() }}" width="50px" height="50px">
                                    <div class="rank"><b>{{ $card->getRank() }}</b></div>
                                </div>
                            @endforeach

                            @if(count($bids) != 4)
                                <div class="col-md-12" id="biddingForm" style="display: @if(Session::get('corePlayer') == $playerCards[0]->name) block @else none @endif ;" >
                                    <form action="bidPlace" method="post">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <label for="bid">Bid Your Point:</label>
                                            <input type="number" class="form-control" name="playerBid" id="bid">
                                        </div>
                                        <button type="button" class="btn btn-primary" id="bidPlaceBtn">submit</button>
                                    </form>
                                </div>
                            @endif
                        @else
                            <div style="width: 50px; float: left">
                                <div>
                                    <img src="images/playing-card-back.jpg" width="50px">
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <?php $i++; ?>
        @endforeach
    </div>

    <div class="played" style="position: fixed; top: 400px; left: 800px; background: transparent; width: 300px; height: 300px;">
        <table>
            <tr>
                <td></td>
                <td>
                    <div class="card-holder22" id="played1">
                        <img src="" id="played_{{ $playerCards[0]->name }}_img" width="50px" height="50px">
                        <div class="rank" id="played_{{ $playerCards[0]->name }}_rank"></div>
                    </div>
                </td>
                <td></td>
            </tr>
            <tr>
                <td>
                    <div class="card-holder22" id="played2">
                        <img src="" id="played_{{ $playerCards[1]->name }}_img" width="50px" height="50px">
                        <div class="rank" id="played_{{ $playerCards[1]->name }}_rank"></div>
                    </div>
                </td>
                <td></td>
                <td>
                    <div class="card-holder22" id="played3">
                        <img src="" id="played_{{ $playerCards[2]->name }}_img" width="50px" height="50px">
                        <div class="rank" id="played_{{ $playerCards[2]->name }}_rank"></div>
                    </div>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <div class="card-holder22" id="played4">
                        <img src="" id="played_{{ $playerCards[3]->name }}_img" width="50px" height="50px">
                        <div class="rank" id="played_{{ $playerCards[3]->name }}_rank"></div>
                    </div>
                </td>
                <td></td>
            </tr>
        </table>
    </div>
@stop

@section('scripts')
    <script>
        /*Card Image Name*/
        var images = {
            'Diamond'   : "images/diamond.png",
            'Hearts'    : "images/hearts.png",
            'Spades'    : "images/spade.png",
            'Clubs'     : "images/clubs.png"
        };

        var my_name = '{{ Session::get('corePlayer') }}';
        var all_players = JSON.parse('{!! Cache::get('Players') !!}');

        /*Placeing Bid by Player*/
        $("#bidPlaceBtn").click(function() {
            var bid = $('#bid').val();
            if(bid == 0 || bid == '') {
                return;
            }
            $.get( "/bidPlace/" + bid, function( data ) {
                $('#biddingForm').hide();
            });
        });

        /*Check User Bid Point at a spacific time preiod*/
        var checkInterval = setInterval(function(){
            $.get( "/check-bids", function( data ) {
                var nextIndex = Object.keys(data).length;
                var next = all_players[nextIndex];

                //console.log(nextIndex);

                var tableData = '';
                for (var x in data) {
                    tableData +=  "<tr><td>" + x + " - " + data[x] + "</td></tr>";
                }
                $('#playerBids').html(tableData);

                if(next == my_name) {
                    $('#biddingForm').show();
                }

                if(Object.keys(data).length == 4) {
                    if(all_players[0] == my_name) {
                        $('.card-holder').addClass('playable');
                    }
                    clearInterval(checkInterval);
                }
            });
        }, 1000);

        playerCircle = all_players;

        setInterval(function(){
            $.get( "/check-play", function( data ) {

                console.log(data);
                var nextIndex = Object.keys(data).length;
                var next = playerCircle[nextIndex];

                for(var played in data) {
                    var suite = data[played].suite;
                    var rank = data[played].rank;
                    $('#played_' + played + '_img').attr('src', images[suite]);
                    $('#played_' + played + '_rank').html("<b>" + rank + "</b>");
                }

                if(next == my_name) {
                    $('.card-holder').addClass('playable');
                }

                if(Object.keys(data).length == 4) {
                    $.get( "/calculate", function( data ) {
                        //console.log(data);
                        var tableData = '';
                        for (var x in data.points) {
                            tableData +=  "<tr><td>" + x + " - " + data[x] + "</td></tr>";
                        }
                        $('#playerCollected').html(tableData);

                        for(var i=0; i<=3; i++) {
                            $('#played_' + all_players[i] + '_img').removeAttr('src');
                            $('#played_' + all_players[i] + '_rank').html("");
                        }

                        if(data.next == my_name) {
                            $('.card-holder').addClass('playable');
                        }

                        nextIndex = jQuery.inArray( data.next, all_players );
                        playerCircle[0] = data.next;
                        for(i=1; i<4; i++) {
                            nextIndex++;
                            if(nextIndex == 4) nextIndex = 0;
                            playerCircle[i] = all_players[nextIndex];
                        }
                    });
                }
            });
        }, 1000);

        /*Clickable Card indivisual User*/
        $(document).delegate('.playable', 'click', function() {
            var rank = $(this).attr('data-rank');
            var suite = $(this).attr('data-suite');

            $('#played_' + my_name + '_img').attr('src', images[suite]);
            $('#played_' + my_name + '_rank').html("<b>" + rank + "</b>");
            $(this).remove();

            $.get( "/play-card/" + suite + "/" + rank, function( data ) {
                $('.card-holder').removeClass('playable');
            });
        });
    </script>
@stop