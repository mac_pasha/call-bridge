@extends('layouts.application')

@section('content')
    <div class="row">
        <?php $players = json_decode(Cache::get('Players')); ?>
        @foreach ($players as $player)
            <li>{{ $player }}</li>
        @endforeach
    </div>

    <script>
        //console.log( 'http://' + window.location.host);
        setInterval(function(){
            $.get( "/test-players", function( data ) {
                if(data.count == 4) {
                    window.location.href = 'http://' + window.location.host + '/shuffle';
                }
            });
        }, 1000);
    </script>
@stop
